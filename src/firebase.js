import  * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';


const config  = {
    apiKey: "AIzaSyB2wdIOqjzxvNrTenCGuTJQeQLLw768_iA",
    authDomain: "goalcoch-63b67.firebaseapp.com",
    databaseURL: "https://goalcoch-63b67.firebaseio.com",
    projectId: "goalcoch-63b67",
    storageBucket: "",
    messagingSenderId: "173604005398"
    
  };

 export const firebaseApp = firebase.initializeApp(config);
 export const goalRef = firebase.database().ref('goals');
